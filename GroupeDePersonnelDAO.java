import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class GroupeDePersonnelDAO extends DAO<GroupeDePersonnels>{
	File fichier;
	ObjectOutputStream oos;
	ObjectInputStream ois;
	@Override
	public boolean create(GroupeDePersonnels obj) {
		
		try {
			oos.writeObject(obj);
			return true;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean delete(GroupeDePersonnels obj) {
		
		return false;
	}

	@Override
	public boolean update(GroupeDePersonnels obj) {
		
		return false;
	}

	@Override
	public void init() {
		File fichier = new File("GroupePersonneDAO.ser");
		try {
			oos = new ObjectOutputStream(new FileOutputStream(fichier));
			ois = new ObjectInputStream(new FileInputStream(fichier));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	@Override
	public GroupeDePersonnels find() {
		GroupeDePersonnels groupePersonne = null;
		try {
			groupePersonne = (GroupeDePersonnels)ois.readObject();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return groupePersonne;
	}

}
