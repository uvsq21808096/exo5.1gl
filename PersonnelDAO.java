import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class PersonnelDAO extends DAO<Personnel>{
	File fichier;
	ObjectOutputStream oos;
	ObjectInputStream ois;
	@Override
	public boolean create(Personnel obj) {
		try {
			oos.writeObject(obj);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;

	}

	@Override
	public boolean delete(Personnel obj) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean update(Personnel obj) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Personnel find() {
		Personnel p = null;
		try {
			p = (Personnel)ois.readObject();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return p;
	}

	@Override
	public void init() {
		File fichier = new File("PersonnelDAO.ser");
		try {
			oos = new ObjectOutputStream(new FileOutputStream(fichier));
			ois = new ObjectInputStream(new FileInputStream(fichier));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
