import static org.junit.Assert.*;

import org.junit.Test;

public class TestPersonnel {

	@Test
	public void InstanceNom() {
		NumeroTelephone tel1 = new NumeroTelephone("1234");
		Personnel p1 = new Personnel.Builder("Nom","prenom", "PDG").builderNumTel(tel1).build();
		assertEquals(p1.getNom(), "Nom");
		assertEquals(p1.getPrenom(), "prenom");
	}
	@Test
	public void InstancePrenom() {
		NumeroTelephone tel1 = new NumeroTelephone("1234");
		Personnel p1 = new Personnel.Builder("Nom","prenom", "PDG").builderNumTel(tel1).build();
		assertEquals(p1.getPrenom(), "prenom");
	}
	@Test
	public void InstanceTelephone() {
		NumeroTelephone tel1 = new NumeroTelephone("1234");
		Personnel p1 = new Personnel.Builder("Nom","prenom", "PDG").builderNumTel(tel1).build();
		assertEquals(tel1, p1.getTelephone());
	}
	@Test
	public void TestDAOPersonne() {
		DAO<Personnel> personnelDao = new PersonnelDAO();
		personnelDao.init();
		Personnel p1 = new Personnel.Builder("Jean","Luck", "PDG").build();
		personnelDao.create(p1);
		Personnel p = personnelDao.find();
		assertEquals(p1.getNom(), p.getNom());
	}
	@Test
	public void TestDAOGroupePersonne() {
		DAO<GroupeDePersonnels> groupeDePersonneDao = new GroupeDePersonnelDAO();
		groupeDePersonneDao.init();
		Conponement p1 = new Personnel.Builder("Jean","Luck", "PDG").build();
		Conponement p2 = new Personnel.Builder("Bastien","Alain", "SG").build();
		GroupeDePersonnels g1 = new GroupeDePersonnels("RH");
		g1.ajouter(p1);
		g1.ajouter(p2);
		groupeDePersonneDao.create(g1);
		GroupeDePersonnels g2 = groupeDePersonneDao.find();
		assertEquals(g2.getList().size(),2);
	}
	@Test
	public void TestDAOGroupePersonneAvecGroupe() {
		DAO<GroupeDePersonnels> groupeDePersonneDao = new GroupeDePersonnelDAO();
		groupeDePersonneDao.init();
		Conponement p1 = new Personnel.Builder("Jean","Luck", "PDG").build();
		Conponement p2 = new Personnel.Builder("Bastien","Alain", "SG").build();
		Conponement p3 = new Personnel.Builder("Cecile","Longchamp", "Secretaire").build();
		GroupeDePersonnels g1 = new GroupeDePersonnels("RH");
		GroupeDePersonnels g2 = new GroupeDePersonnels("Scolarité");
		g1.ajouter(p1);
		g1.ajouter(p2);
		g2.ajouter(p3);
		g1.ajouter(g2);
		groupeDePersonneDao.create(g1);
		GroupeDePersonnels g3 = groupeDePersonneDao.find();
		int x=0;
		GroupeDePersonnels g4;
		for (Conponement t : g3.getList()) {
			if(t.getClass() == GroupeDePersonnels.class)
			{
				g4 = (GroupeDePersonnels) t;
				x = x + g4.getList().size();
			}
			else 
			{
				x++;
			}
			
		}
		//assertEquals(g1.getList().size(),3);
		assertEquals(x,3);
	}

}
