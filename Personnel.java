import java.io.Serializable;
import java.time.LocalDate;

public class Personnel implements Affichage, Conponement, Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final String nom;
	final String prenom;
	final String fonction;
	final java.time.LocalDate dateNaissance;
	final NumeroTelephone telephone;
	
	public static class Builder
	{
		private final String nom;
		private final String prenom;
		private final String fonction;
		private NumeroTelephone telephone;
		private java.time.LocalDate dateNaissance;
		
		public Builder(String nom, String prenom, String fonction)
		{
			this.nom = nom;
			this.prenom = prenom;
			this.fonction = fonction;
		}
		public Personnel build()
		{
			return new Personnel(this);
		}
		
		public Builder builderNumTel(NumeroTelephone tel)
		{
			telephone = tel;
			return this;
		}
		
		public Builder builderDate(LocalDate t)
		{
			dateNaissance = t;
			return this;
		}
		
	}
	
	private Personnel(Builder builder)
	{
		nom = builder.nom;
		prenom = builder.prenom;
		fonction = builder.fonction;
		telephone = builder.telephone;
		dateNaissance = builder.dateNaissance;
	}

	@Override
	public void print() {
		System.out.println("Je suis un membre personnel nom="+nom+" prenom="+prenom+" fonction="+fonction);
	}

	public String getNom() {
		return nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public String getFonction() {
		return fonction;
	}

	public java.time.LocalDate getDateNaissance() {
		return dateNaissance;
	}

	public NumeroTelephone getTelephone() {
		return telephone;
	}

	@Override
	public void ajouter(Conponement conponement){
		conponement.groupe.add(this);
	}
}
