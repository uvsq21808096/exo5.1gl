import java.io.Serializable;

public class NumeroPortable extends NumeroTelephone implements Serializable{
	String indicatif="";
	
	public NumeroPortable(String num, String indicatif) {
		super(num);
		this.indicatif = indicatif;	
	}
	
	public String getNumero() {
		String x; 
		x = new StringBuilder().append(indicatif ).append(super.getNumero()).toString();
		return x;
	}

	public String getIndicatif() {
		return indicatif;
	}

}
